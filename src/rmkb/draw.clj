(ns rmkb.draw
  (:require
   [rmkb.game :as game]
   [rmkb.util :as util]
   [malli.core :as m])
  (:import
   (java.awt BasicStroke Color Font LinearGradientPaint Polygon)
   (java.awt.font TextAttribute TextLayout)
   (java.awt.geom Area Rectangle2D$Float Path2D$Float)))

(def default-settings
  {::colors {::game/red (Color. 255 20 98)
             ::game/black (Color. 255 52 153)
             ::game/yellow (Color. 255 208 146)
             ::game/blue (Color. 150 187 241)
             ::white (Color. 243 232 238)
             ::background (Color. 32 0 31)
             ::tile-background (Color. 32 0 31)
             ::tile-background-selected (Color. 66 22 79 216)
             ::handle-left {::unfocused (Color. 124 59 161)
                            ::focused   (Color. 124 59 161)
                            ::depressed (Color. 124 59 161)}
             ::handle-right {::unfocused (Color. 124 59 161 96)
                             ::focused   (Color. 124 59 161)
                             ::depressed (Color. 200 110 124)}}
   ::symbol {::bar {::length 1.05
                    ::thickness 0.125
                    ::offset 0.1
                    ::pointing-adjustment {::up 0.2 ::down 0.015}}
             ::side-length 0.54
             ::joker-scale 0.8
             ::thickness 0.125}
   ::numeral {::typeface "Caladea"
              ::size {::big 52 ::small 42}
              ::joker {::height 0.34
                       ::thickness 0.07}}
   ::tile-geometry {::width 80
                    ::height 160
                    ::thickness 0.08
                    ::top-margin 0.105
                    ::bottom-margin 0.07
                    ;; Optical compensation for the Müller-Lyer effect
                    ::pointing-adjustment {::up 0 ::down 0.02 nil 0}
                    ::padding 0.2}
   ::handle {::top-gap 0.05
             ::left-gap 0.09
             ::thickness 0.08
             ::drop 0.12}})

(def settings (atom default-settings))

(def color->pointing
  {::game/red ::up
   ::game/yellow ::up
   ::game/blue ::down
   ::game/black ::down})

(defn tile-geometry []
  (let [{:keys [::width ::height ::padding ::top-margin ::bottom-margin ::thickness ::pointing-adjustment]} (::tile-geometry @settings)]
    {::width width
     ::height height
     ::padding (* padding width)
     ::top-margin (* top-margin height)
     ::bottom-margin (* bottom-margin height)
     ::thickness (* thickness width)
     ::pointing-adjustment (update-vals pointing-adjustment #(* % width))}))

(m/=> tile-geometry [:=> [:cat]
                     [:map [::width number?]
                           [::height number?]
                           [::padding number?]
                           [::top-margin number?]
                           [::bottom-margin number?]
                           [::thickness number?]
                           [::pointing-adjustment number?]]])

(def Position [:tuple number? number?])

(defn draw-background [gx size]
  (doto gx
    (.setBackground (-> @settings ::colors ::background))
    (.clearRect 0 0 (.getWidth size) (.getHeight size))))

(defn equilateral-triangle
  "Points of an equilateral triangle whose center (meaning midpoint of vertical altitude) is at (x,y)"
  ([x y side-length] (equilateral-triangle x y 0 side-length))
  ([x y phase side-length]
   (let [angle (* 2/3 Math/PI)
         ;; It's most convenient to draw from the circumcenter, so we need to turn our bad but
         ;; visually-convenient triangle center into a circumcenter. We can do this by going "up" by
         ;; the radius from the weird-center to the apex of the triangle, and then "down" by half
         ;; the altitude. By linearity, this is equivalent to going "up" by the radius minus half
         ;; the altitude.
         radius (/ side-length (Math/sqrt 3)) ; circumradius
         altitude (* side-length (/ (Math/sqrt 3) 2))
         up-down-distance (- radius (/ altitude 2))
         [center-x center-y] [(+ x (* up-down-distance (Math/sin phase)))
                              (+ y (* up-down-distance (Math/cos phase)))]]
     (for [k (range 3)]
       [(- center-x (* radius (Math/sin (+ phase (* k angle)))))
        (- center-y (* radius (Math/cos (+ phase (* k angle)))))]))))

(defn draw-element-symbol [gx color center]
  (let [[center-x center-y] center
        side-length (* (-> @settings ::symbol ::side-length)
                       (-> @settings ::tile-geometry ::width))
        phase (if (= (color->pointing color) ::down)
                Math/PI 0)
        points (equilateral-triangle center-x center-y phase side-length)
        xpoints (map (comp #(Math/round %) first) points)
        ypoints (map (comp #(Math/round %) second) points)
        triangle (Polygon. (int-array xpoints) (int-array ypoints) 3)
        bar? (#{::game/black ::game/yellow} color)
        pointing-adjustment ((-> @settings ::symbol ::bar ::pointing-adjustment)
                             (color->pointing color))
        bar-y (+ center-y (* pointing-adjustment side-length (-> @settings ::symbol ::bar ::offset)))
        bar-radius (* 1/2 side-length (-> @settings ::symbol ::bar ::length))]
    (doto gx
      (.setColor (-> @settings ::colors color))
      (.setStroke (BasicStroke. (* side-length (-> @settings ::symbol ::thickness))
                                BasicStroke/CAP_BUTT
                                BasicStroke/JOIN_MITER))
      (.draw triangle))
    (when bar?
      (doto gx
        (.setStroke (BasicStroke. (* side-length (-> @settings ::symbol ::bar ::thickness))
                                  BasicStroke/CAP_BUTT
                                  BasicStroke/JOIN_MITER))
        (.drawLine (- center-x bar-radius) bar-y (+ center-x bar-radius) bar-y)))))

(defn draw-joker-numeral [gx [x y]]
  (let [{:keys [::height ::thickness]} (-> @settings ::numeral ::joker)
        height (* height (-> @settings ::tile-geometry ::height))
        thickness (* thickness (-> @settings ::tile-geometry ::width))
        radius (* height 1/3)
        line-start-x (+ x (* radius -0.34797))
        line-start-y (+ y radius (* radius 0.9380))
        line-end-x (+ line-start-x (* radius -0.8096))
        line-end-y (+ line-start-y (* radius 1.0552))
        swish (->> [0 0, 0.393074 -0.29013, 0.664483 -0.29013, 0.935892 -0.29013, 1.502106 -0.051478, 1.839027 -0.051478]
                   (map #(* % radius))
                   (map + (interleave (repeat line-end-x) (repeat line-end-y))))]
    (doto gx
      (.setColor (-> @settings ::colors ::white))
      (.setStroke (BasicStroke. thickness BasicStroke/CAP_BUTT BasicStroke/JOIN_MITER))
      (.drawOval (- x radius) y (* 2 radius) (* 2 radius))
      (.setStroke (BasicStroke. thickness BasicStroke/CAP_BUTT BasicStroke/JOIN_BEVEL))
      (.draw (doto (Path2D$Float.)
               (.moveTo line-start-x line-start-y)
               (.lineTo line-end-x line-end-y)
               (as-> path (apply Path2D$Float/.curveTo path (take 6 swish)))
               (as-> path (apply Path2D$Float/.curveTo path (drop 6 swish))))))))

(defn draw-joker-symbol [gx [center-x center-y]]
  (let [side-length (* (-> @settings ::tile-geometry ::width)
                       (-> @settings ::symbol ::side-length)
                       (-> @settings ::symbol ::joker-scale))]
    (doto gx
      (.setColor (-> @settings ::colors ::white))
      (.drawLine (- center-x (/ side-length 2)) (- center-y (/ side-length 2))
                 (+ center-x (/ side-length 2)) (+ center-y (/ side-length 2)))
      (.drawLine (- center-x (/ side-length 2)) (+ center-y (/ side-length 2))
                 (+ center-x (/ side-length 2)) (- center-y (/ side-length 2))))))

(defn draw-numeral
  "Draws the number with the middle of its top at (x,y)"
  [gx color value position]
  (let [[x y] position
        {:keys [::typeface ::size]} (get @settings ::numeral)
        size (if (< value 10) (::big size) (::small size))
        size (* size 1/50 (-> @settings ::tile-geometry ::width))
        font (.deriveFont (Font. typeface Font/PLAIN size) {TextAttribute/TRACKING -30.0})
        layout (TextLayout. (str value) font (.getFontRenderContext gx))
        bounds (.getBounds layout)]
    (.setColor gx (-> @settings ::colors color))
    (.draw layout gx (- x (.getX bounds) (/ (.getWidth bounds) 2))
                     (- y (.getY bounds)))))

(defn draw-tile
  ([gx tile position] (draw-tile gx tile position (constantly false)))
  ([gx tile [x y] selected]
   (let [{:keys [::game/value ::game/color]} tile
         {:keys [::width ::height ::top-margin ::bottom-margin ::thickness ::pointing-adjustment]} (tile-geometry)
         symbol-side-length (* width (-> @settings ::symbol ::side-length))
         background (if (selected tile)
                      (-> @settings ::colors ::tile-background-selected)
                      (-> @settings ::colors ::tile-background))
         numeral-position [(+ x (/ width 2)) (+ y top-margin)]
         symbol-position [(+ x (/ width 2))
                          (+ y height
                             (- bottom-margin)
                             (- thickness)
                             ;; halfway back up the triangle; altitude is sqrt(3)/2*s
                             (- (* (/ (Math/sqrt 3) 4) symbol-side-length))
                             (- (pointing-adjustment (color->pointing color))))]]
     (doto gx
       (.setColor background)
       (.fillRect x y width height)
       (.setColor (-> @settings ::colors ::white))
       (.setStroke (BasicStroke. thickness BasicStroke/CAP_BUTT BasicStroke/JOIN_MITER))
       (.drawRect x y width height))
     (if (::game/joker tile)
       (doto gx
         (draw-joker-numeral numeral-position)
         (draw-joker-symbol symbol-position))
       (doto gx
         (draw-numeral color value numeral-position)
         (draw-element-symbol color symbol-position))))))

(m/=> draw-tile [:=> [:cat [:fn #(instance? java.awt.Graphics2D %)]
                           game/Tile
                           Position
                           [:? [:=> [:cat game/Tile] :any]]]
                     :any])

(defn tile-hitbox [[x y]]
  (let [{:keys [::width ::height]} (tile-geometry)]
    (Rectangle2D$Float. x y width height)))

(defn draw-tiles
  ([gx tiles position] (draw-tiles gx tiles position (constantly false)))
  ([gx tiles position selected]
   (let [[x y] position
         {:keys [::width ::padding]} (tile-geometry)
         offset (volatile! 0)]
     (doseq [tile tiles]
       (draw-tile gx tile [(+ x @offset) y] selected)
       (vswap! offset + width padding)))))

(m/=> draw-tiles [:=> [:cat [:fn #(instance? java.awt.Graphics2D %)]
                            [:sequential game/Tile]
                            Position
                            [:? [:=> [:cat game/Tile] :any]]]
                      :any])

(defn handle-geometry [n-tiles position]
   (let [[x y] position
         {:keys [::top-gap ::left-gap ::drop ::thickness]} (get @settings ::handle)
         {:keys [::padding]} (get @settings ::tile-geometry)
         tile-width (-> @settings ::tile-geometry ::width)
         tile-height (-> @settings ::tile-geometry ::height)
         top-gap (* top-gap tile-width)
         drop (* drop tile-height)
         left-gap (* left-gap tile-width)
         thickness (* thickness tile-width)
         left (- x left-gap (/ thickness 2))
         padded-tile-width (+ tile-width (* padding tile-width))
         right (+ x (* n-tiles padded-tile-width))
         top (- y top-gap (/ thickness 2))]
     (util/un-keys ::left ::top ::right ::left-gap ::top-gap ::drop ::thickness)))

(m/=> handle-geometry [:=>
                       [:cat nat-int? Position]
                       [:map
                        [::left number?]
                        [::top number?]
                        [::right number?]
                        [::left-gap number?]
                        [::top-gap number?]
                        [::drop number?]
                        [::thickness number?]]])

(defn draw-handle
  "Returns the handle's two hitboxes; the first hitbox is for the left overhang, and the second is
  the main top part. These hitboxesa may overlap."
  ([gx n-tiles position]
   (draw-handle gx ::unfocused n-tiles position))
  ([gx state n-tiles position]
   (let [{:keys [::handle-left ::handle-right]} (::colors @settings)]
     (assert (#{::unfocused ::focused ::depressed} state))
     (draw-handle gx (get handle-left state) (get handle-right state) n-tiles position)))
  ([gx left-color right-color n-tiles position]
   (let [{:keys [::left ::top ::right ::left-gap ::top-gap ::thickness ::drop]} (handle-geometry n-tiles position)
         gradient (LinearGradientPaint. left 0 right 0
                                        (float-array [0. 1.])
                                        (into-array Color [left-color right-color]))]
     (doto gx
       (.setPaint gradient)
       (.setStroke (BasicStroke. thickness BasicStroke/CAP_BUTT BasicStroke/JOIN_MITER))
       (.drawPolyline (int-array [left left right])
                      (int-array [(+ top drop)
                                  (- top top-gap)
                                  (- top top-gap)])
                      3)))))

(m/=> draw-handle [:=> [:cat [:fn #(instance? java.awt.Graphics2D %)]
                             [:alt [:? [:enum ::unfocused ::focused ::depressed]]
                                   [:cat [:fn #(instance? java.awt.Color %)]
                                         [:fn #(instance? java.awt.Color %)]]]
                             pos-int?
                             Position]
                       :any])

(defn handle-hitbox [n-tiles position]
  (let [{:keys [::left ::top ::right ::drop ::left-gap ::top-gap ::thickness]} (handle-geometry n-tiles position)]
    (doto (Area.)
      (.add (Area. (Rectangle2D$Float. (- left (/ thickness 2)) (- top thickness) (- right left) (+ thickness top-gap))))
      (.add (Area. (Rectangle2D$Float. (- left (/ thickness 2)) (- top thickness) (+ (/ thickness 2) left-gap) (+ thickness drop)))))))

(m/=> handle-hitbox [:=> [:cat pos-int? Position] [:fn #(instance? java.awt.Shape %)]])

(defmacro -config []
  '(do
     (def tileseq (conj (for [color [::game/red ::game/blue ::game/yellow ::game/black]]
                          {::game/color color ::game/value 7 ::game/uuid (java.util.UUID/randomUUID)})
                        {::game/joker true ::game/uuid (java.util.UUID/randomUUID)}))
     (def state (atom {::hovering? ::unfocused}))
     (def tilepos [60 60])
     (defn paint-component [gx]
       (.setBackground gx (-> @settings ::colors ::background))
       (.clearRect gx 0 0 800 700)
       (.setColor gx (Color. 200 200 200))
       (.drawLine gx 0 150 700 150)
       (draw-tiles gx tileseq tilepos #(or (::game/joker %) (#{::game/blue ::game/black} (::game/color %))))
       (draw-handle gx (::hovering? @state) (count tileseq) tilepos))))

(defmacro -setup []
  '(do
     (import javax.swing.JPanel)
     (import javax.swing.JFrame)
     (import java.awt.Dimension)
     (import java.awt.geom.Point2D$Float)
     (import java.awt.event.MouseMotionListener)
     (-config)
     (def panel (proxy [JPanel] []
                  (paintComponent [gx]
                    (proxy-super paintComponent gx)
                    (paint-component gx))))
     (.addMouseMotionListener
      panel
      (reify MouseMotionListener
        (mouseMoved [_ ev]
          (if (.contains (handle-hitbox (count tileseq) tilepos)
                         (Point2D$Float. (.getX ev) (.getY ev)))
            (swap! state assoc ::hovering? ::focused)
            (swap! state assoc ::hovering? ::unfocused))
          (.repaint panel))
        (mouseDragged [_ _])))
     (.setPreferredSize panel (Dimension. 800 700))
     (def frame (JFrame.))
     (doto frame
       (.add panel)
       .pack
       (.setVisible true))
     (def gx (.getGraphics panel))))
