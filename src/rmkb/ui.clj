(ns rmkb.ui
  (:require
   [rmkb.game :as game]
   [rmkb.draw :as draw]
   [clojure.core.match :refer [match]]
   [malli.core :as m]
   [clojure.pprint :refer [pprint]])
  (:import
   (java.awt RenderingHints)
   (java.awt.geom Point2D$Float)
   (java.awt.event MouseEvent)))

(def Position [:tuple number? number?])

(def TileGroup [:map [::uuid :uuid]
                     [::position Position]
                     [::tiles [:sequential game/Tile]]])

(def TileGroupId :uuid)

(def TileId :uuid)

(def TileGroupMap [:and [:map-of TileGroupId TileGroup]
                        [:fn (partial every? (fn [[id group]] (= id (::uuid group))))]])

(def GameState [:map [::tile-groups TileGroupMap]
                     [::selection [:set TileId]]
                     [::dragging-group [:maybe TileGroupId]]
                     [::drag-start [:maybe Position]]
                     [::last-drag-position [:maybe draw/Position]]])

;; TODO for testing only. remove later.
(defonce game-state
  (atom {::tile-groups {}
         ::selection #{}
         ::dragging-group nil
         ::drag-start nil
         ::last-drag-position nil}
        :validator #(m/validate GameState %)))

(defonce view-state
  (atom {::camera-pos [0 0]
         ::zoom 1
         ::mouse-position [0 0]}))

(defn gamepos->drawpos [position]
  (let [{:keys [::camera-pos ::zoom]} @view-state]
    (->> position
         (mapv + camera-pos)
         (map #(/ % zoom)))))

(m/=> gamepos->drawpos [:=> [:cat Position] draw/Position])

(defn drawpos->gamepos [position]
  (let [{:keys [::camera-pos ::zoom]} @view-state]
    (->> position
         (map #(* zoom %))
         (mapv #(- % camera-pos)))))

(m/=> drawpos->gamepos [:=> [:cat draw/Position] Position])

(defn ->point [[x y]] (Point2D$Float. x y))

(defn draw-tiles [gx]
  (doseq [{:keys [::tiles ::position ::uuid]} (vals (::tile-groups @game-state))
          :let [position (gamepos->drawpos position)
                selection (::selection @game-state)
                dragging-group (::dragging-group @game-state)]]
    (draw/draw-tiles gx tiles (gamepos->drawpos position) #(selection (::game/uuid %)))
    (when (> (count tiles) 1)
      (draw/draw-handle gx
                        (cond
                         (= uuid dragging-group) ::draw/depressed
                         (.contains (draw/handle-hitbox (count tiles) (gamepos->drawpos position))
                                    (->point (::mouse-position @view-state)))
                         ::draw/focused
                         :else ::draw/unfocused)
                        (count tiles)
                        (gamepos->drawpos position)))))

(defn tile-group-focus-targets [mouse-position]
  (->> (vals (::tile-groups @game-state))
       (filter (fn [{:keys [::position ::tiles]}]
                 (.contains (draw/handle-hitbox (count tiles) (gamepos->drawpos position))
                            (->point mouse-position))))
       (map ::uuid)))

(defn tile-focus-targets [mouse-position]
  (let [{:keys [::draw/width ::draw/padding]} (draw/tile-geometry)]
    (->> (vals (::tile-groups @game-state))
         (mapcat (fn [tile-group]
                   (map-indexed (fn [index tile]
                                  [tile-group index tile])
                                (::tiles tile-group))))
         (filter (fn [[tile-group index _]]
                   (let [group-position (::position tile-group)
                         tile-position (mapv + group-position [(* (+ width padding) index) 0])]
                     (.contains (draw/tile-hitbox (gamepos->drawpos tile-position))
                                (->point mouse-position)))))
         (map (fn [[_ _ tile]]
                (::game/uuid tile))))))

(defn resolve-focus [mouse-position]
  (if-let [group (first (tile-group-focus-targets mouse-position))]
    {::target-type ::tile-group ::target group}
    (when-let [tile (first (tile-focus-targets mouse-position))]
      {::target-type ::tile ::target tile})))

(defn conj-or-disj [coll elem]
  (if (contains? coll elem)
    (disj coll elem)
    (conj coll elem)))

(defn doevent [event]
  (pprint event)
  (match event
    {::type ::mouse-pressed ::button ::mouse-1 ::position position
     ::target-type ::tile-group ::target group-id}
    (swap! game-state assoc ::dragging-group group-id
                            ::drag-start (mapv - position (-> @game-state ::tile-groups (get group-id) ::position)))
    {::type ::mouse-pressed ::button ::mouse-3
     ::target-type ::tile ::target tile-id}
    (swap! game-state update ::selection conj-or-disj tile-id)
    {::type ::mouse-released ::button ::mouse-1}
    (swap! game-state assoc ::dragging-group nil
                            ::drag-start nil)
    {::type ::mouse-dragged ::position position}
    (when-let [group-id (::dragging-group @game-state)]
      (swap! game-state assoc-in [::tile-groups group-id ::position]
                                 (mapv - position (::drag-start @game-state)))
      (swap! view-state assoc ::mouse-position position))
    {::type ::mouse-moved ::position position}
    (swap! view-state assoc ::mouse-position position)
    :else nil))

(defn get-mouse-button [mouse-event]
  (condp = (.getButton mouse-event)
    MouseEvent/NOBUTTON nil
    MouseEvent/BUTTON1 ::mouse-1
    MouseEvent/BUTTON2 ::mouse-2
    MouseEvent/BUTTON3 ::mouse-3))

(m/=> get-mouse-button [:=> [:cat [:fn #(instance? MouseEvent %)]]
                            [:maybe :any]])

(defn get-mouse-position [mouse-event]
  [(.getX mouse-event) (.getY mouse-event)])

(defmacro -setup []
  '(do
     (import javax.swing.JFrame)
     (import javax.swing.JPanel)
     (import java.awt.Color)
     (import java.awt.Dimension)
     (import java.awt.RenderingHints)
     (import java.awt.event.MouseListener)
     (import java.awt.event.MouseMotionListener)
     (def tile-group {::uuid (java.util.UUID/randomUUID)
                      ::position [15 15]
                      ::tiles (into (for [color [::game/red ::game/blue ::game/yellow ::game/black]]
                                      {::game/color color ::game/value 4 ::game/uuid (java.util.UUID/randomUUID)})
                                    [{::game/joker true ::game/uuid (java.util.UUID/randomUUID)}])})
     (when (empty? (::tile-groups @game-state))
       (swap! game-state assoc-in [::tile-groups (::uuid tile-group)] tile-group))
     (def panel (proxy [JPanel] []
                  (paintComponent [gx]
                    (proxy-super paintComponent gx)
                    (.setRenderingHint gx RenderingHints/KEY_ANTIALIASING RenderingHints/VALUE_ANTIALIAS_ON)
                    (.setRenderingHint gx RenderingHints/KEY_TEXT_ANTIALIASING RenderingHints/VALUE_TEXT_ANTIALIAS_ON)
                    (draw/draw-background gx (.getSize this))
                    (draw-tiles gx))))
     (.addMouseListener panel
                        (reify MouseListener
                          (mousePressed [_ ev]
                            (doevent (merge {::type ::mouse-pressed
                                             ::button (get-mouse-button ev)
                                             ::position (get-mouse-position ev)}
                                            (resolve-focus (get-mouse-position ev)))))
                          (mouseReleased [_ ev]
                            (doevent {::type ::mouse-released ::button (get-mouse-button ev)}))
                          (mouseClicked [_ _])
                          (mouseEntered [_ _])
                          (mouseExited [_ _])))
     (.addMouseMotionListener panel
                              (reify MouseMotionListener
                                (mouseMoved [_ ev]
                                  (doevent {::type ::mouse-moved ::position (get-mouse-position ev)}))
                                (mouseDragged [_ ev]
                                  (doevent {::type ::mouse-dragged ::position (get-mouse-position ev)}))))
     (.setPreferredSize panel (Dimension. 800 700))
     (add-watch game-state ::game-state-repaint (fn [& _] (.repaint panel)))
     (add-watch view-state ::view-state-repaint (fn [& _] (.repaint panel)))
     (def frame
       (doto (JFrame.)
         (.add panel)
         .pack
         (.setVisible true)))
     (.repaint panel)
     (def gx (.getGraphics panel))))
