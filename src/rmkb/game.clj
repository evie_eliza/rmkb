(ns rmkb.game
  (:require
   [malli.core :as m]))

(def Value pos-int?)
(def Color [:enum ::black ::blue ::yellow ::red])

(def Tile [:or [:map [::value Value]
                     [::color Color]
                     [::uuid :uuid]]
               [:map [::joker [:enum true]]
                     [::uuid :uuid]]])

(defn find-first-nonjoker [tiles]
  (->> tiles
       (map vector (range)) ; [tile] -> [(index, tile)]
       (some (complement ::joker))))

(defn valid-sequence? [tiles]
  (if-let [[index-first-nonjoker first-nonjoker] (find-first-nonjoker tiles)]
    (->> tiles
         (map vector (range)) ; [tile] -> [(index, tile)]
         (filter (fn [[_index tile]] (not (::joker tile))))
         (every? (fn [[index tile]]
                   (and (= (::color tile) (::color first-nonjoker))
                        (= (::value tile) (+ (::value first-nonjoker)
                                             index
                                             (- index-first-nonjoker)))))))
    true))

(def Sequence [:and
               [:sequential Tile]
               [:fn valid-sequence?]])

(defn valid-set? [tiles]
  (if-let [nonjokers (seq (filter (complement ::joker) tiles))]
    (and (distinct? (map ::color nonjokers))
         (apply = (map ::value nonjokers)))))

(def Set [:and
          [:sequential Tile]
          [:fn valid-set?]])

(defn meld? [tiles]
  (and (<= 3 (count tiles))
       (or (valid-sequence? tiles) (valid-set? tiles))))

(defn value [meld] (transduce (map ::value) + meld))

(m/=> value [:=> [:cat [:sequential Tile]] nat-int?])
