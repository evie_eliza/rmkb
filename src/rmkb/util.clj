(ns rmkb.util)

(defmacro un-keys [& ks]
  (apply merge (for [k ks]
                 (if (qualified-keyword? k)
                   ;; Surely there is a better way to strip the namespace off the keyword
                   (let [unqualified-name (second (re-find #"/(.*)" (str k)))]
                     (assert (some? unqualified-name))
                     {k (symbol unqualified-name)})
                   {(keyword k) k}))))
